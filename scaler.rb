#!/usr/bin/env ruby
# frozen_string_literal: true

require 'json'

source = 'i247.json'
scale  = 3

file   = File.open(source, 'rb')
layout = JSON.parse(file.read)['layout']

scaled = []

layout.each do |row|
  scaledrow = []
  row.each do |color|
    scale.times do
      scaledrow.push color
    end
  end

  scale.times do
    scaled.push scaledrow
  end
end

puts JSON.generate('layout' => scaled)
