#!/usr/bin/env ruby
# frozen_string_literal: true

require 'faye/websocket'
require 'eventmachine'
require 'net/http'
require 'uri'
require 'json'
require 'thread'

startx = 175
starty = 1875
wrapx  = 1
wrapy  = 1
source = 'i247-x3.json'
timer  = 75
repeat = true
random = true

file   = File.open(source, 'rb')
layout = JSON.parse(file.read)['layout']

ysize  = layout.size
xsize  = layout.first.size

jobs   = Queue.new

ywraps = 0
while ywraps < wrapy
  xwraps = 0
  while xwraps < wrapx
    job = []
    y = 0
    layout.each do |row|
      x = 0
      row.each do |color|
        job.push [startx + x + (xwraps * xsize), starty + y + (ywraps * ysize), color] if color != -1
        x += 1
      end
      y += 1
    end
    job.reverse! if random
    job.each do |j|
      jobs.push j
    end
    xwraps += 1
  end
  ywraps += 1
end

onetime = (jobs.length * timer) / 60 / 60
puts "Start X        : #{startx}"
puts "Start Y        : #{starty}"
puts "Drawing from   : #{source}"
puts "Repeat draw    : #{repeat}"
puts "Randomize draw : #{random}"
puts "Duplicate X    : #{wrapx}"
puts "Duplicate Y    : #{wrapy}"
puts "Pixels to draw : #{jobs.length}"
puts "Draw time      : #{onetime} hours"

def connect
  wsuri = JSON.parse(Net::HTTP.get(URI.parse('http://pixelcanvas.io/api/ws')))['url']
  puts "Using endpoint: #{wsuri}"
  Faye::WebSocket::Client.new(wsuri)
end

EM.run do
  ws = connect

  ws.on :open do |_event|
    puts 'Connected'
  end

  ws.on :close do |event|
    puts "Lost connection: #{event.code} (#{event.reason})"
    ws = connect
  end

  EventMachine.add_periodic_timer(timer) do
    pixel = jobs.pop

    nx = if pixel[0].negative?
           65_536 + pixel[0]
         else
           pixel[0]
         end

    ny = if pixel[1].negative?
           65_536 + pixel[1]
         else
           pixel[1]
         end

    head = 1.to_s(16).rjust(2, '0').hex.chr
    x    = nx.to_s(16).rjust(4, '0').scan(/../).map { |c| c.hex.chr }.join
    y    = ny.to_s(16).rjust(4, '0').scan(/../).map { |c| c.hex.chr }.join
    col  = pixel[2].to_s(16).rjust(2, '0').hex.chr

    payload = "#{head}#{x}#{y}#{col}"

    puts "Painting (#{pixel[0]},#{pixel[1]}): #{pixel[2]}"

    begin
      ws.send payload.unpack('C*')
    rescue StandardError => e
      puts "Error #{e}"
    end

    jobs.push pixel if repeat
  end
end
